var Startpage = cc.Sprite.extend({
  init: function(){
    this._super();
    this.initWithFile('res/image/end.png');
    this.setPosition(800,250);
    this.addKeyboardHandlers();
  },
  addKeyboardHandlers: function() {
  var self = this;
  cc.eventManager.addListener({
    event: cc.EventListener.KEYBOARD,
    onKeyPressed : function( keyCode, event ) {
    self.onKeyDown( keyCode, event );
  },
  }, this);
},
  onKeyDown: function( keyCode, event ) {
  if(keyCode == 32){
    console.log(32);
    cc.director.runScene(new StartScene());
  }
},
});
var FirstScene = cc.Scene.extend({
  onEnter: function() {
  this._super();

  var layer = new Startpage();
  layer.init();
  this.addChild( layer );
}
});
