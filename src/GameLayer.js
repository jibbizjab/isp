var GameLayer = cc.LayerColor.extend({
	init: function() {
	this._super( new cc.Color( 127, 127, 127, 255 ) );

	this.creatBackground();
	this.creatPlayer();
	this.creatBox();
	this.scheduleUpdate();
  this.addKeyboardHandlers();
  return true;
	},
	update: function(){
		score++;
		this.isPlayerHitBox();
		this.player1.crashPlayer(this.player2);
},
	creatBackground: function(){
	var audio = cc.audioEngine;
	audio.playEffect('res/sound/Space_Traveler.mp3');
	this.background = new cc.Sprite(res.background);
	this.addChild(this.background);
	this.background.setPosition( new cc.Point( 800, 250 ) );
	this.setPosition( new cc.Point( 0, 0 ) );
},
	creatPlayer: function(){
		this.player1 = new Player();
		this.player1.setImage('res/image/takman.png');
	  this.player1.setPosition(450,150);
		this.player1.scheduleUpdate();
		this.addChild(this.player1);

		this.player2 = new Player();
		this.player2.setImage('res/image/tank.png');
	  this.player2.setPosition(150,150);
		this.player2.scheduleUpdate();
		this.addChild(this.player2);
	},
	creatBox: function (){
		this.box1 = new Box();
	  this.box1.setPosition(1700,150);
		this.box1.scheduleUpdate();
		this.addChild(this.box1);
		this.box2 = new Box();
	  this.box2.setPosition(2200,350);
		this.box2.scheduleUpdate();
		this.addChild(this.box2);
	},
	isPlayerHitBox: function(){
		if( this.box1.hitPlayer(this.player1,"TANK WIN") && this.box2.hitPlayer(this.player1,"TANK WIN") ){
			this.unscheduleUpdate();
		}
		if(this.box1.hitPlayer(this.player2,"TAKMAN WIN") && this.box2.hitPlayer(this.player2,"TAKMAN WIN")){
			this.unscheduleUpdate();
		}
	},
	addKeyboardHandlers: function() {
	var self = this;
	cc.eventManager.addListener({
		event: cc.EventListener.KEYBOARD,
		onKeyPressed : function( keyCode, event ) {
		self.onKeyDown( keyCode, event );
	},
	}, this);
},
	onKeyDown: function( keyCode, event ) {
	if(keyCode == cc.KEY.up){
		this.player1.flip();
	}
	else if(keyCode == cc.KEY.left){
		this.player1.moveLeft();
	}
	else if(keyCode == cc.KEY.right){
		this.player1.moveRight();
	}
	else if(keyCode == cc.KEY.w){
		this.player2.flip();
	}
	else if(keyCode == cc.KEY.a){
		this.player2.moveLeft();
	}
	else if(keyCode == cc.KEY.d){
		this.player2.moveRight();
	}
},
});
var score = 0;
var StartScene = cc.Scene.extend({
	onEnter: function() {
	this._super();

	var layer = new GameLayer();
	layer.init();
	this.addChild( layer );
}
});
