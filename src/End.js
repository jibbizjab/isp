var Endpage = cc.Sprite.extend({
  init: function(){
    this._super();
    this.initWithFile('res/image/endscence.png');
    this.setPosition(800,250);
    this.addKeyboardHandlers();
    this.creat_label();
    var audio = cc.audioEngine;
  	audio.playEffect('res/sound/ending.mp3');
  },
  creat_label: function(){
	this.scoreLabel = cc.LabelTTF.create( 'SO NOOB \n SPACE BAR TO RESTART', 'Arial', 40 );
  this.scoreLabel.setPosition( new cc.Point( 800, 250 ) );
	this.addChild( this.scoreLabel );
       },
  addKeyboardHandlers: function() {
  var self = this;
  cc.eventManager.addListener({
    event: cc.EventListener.KEYBOARD,
    onKeyPressed : function( keyCode, event ) {
    self.onKeyDown( keyCode, event );
  },
  }, this);
},
  onKeyDown: function( keyCode, event ) {
  if(keyCode == 32){
    console.log(32);
    cc.director.runScene(new FirstScene());
  }
},
});
var LastScene = cc.Scene.extend({
  onEnter: function() {
  this._super();

  var layer = new Endpage();
  layer.init();
  this.addChild( layer );
}
});
