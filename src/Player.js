var Player = cc.Sprite.extend({
	ctor: function() {
	this._super();
  this.veloX = 60;
	this.veloY = 0;
  this.state = 1;
},
	crashPlayer: function(obj){
		if(Math.abs(this.getPositionX()-obj.getPositionX())<=90  &&  Math.abs(this.getPositionY()-obj.getPositionY())<=90){
			obj.setPositionX(obj.getPositionX()+this.veloY);
			obj.setPositionY(obj.getPositionY()+this.veloY);
			this.setPositionX(this.getPositionX()+this.veloY);
			this.setPositionY(this.getPositionY()+this.veloY);
		}
	},
	setImage: function( imagePath ){
		this.initWithFile( imagePath );
	},
	update: function(dt){
			this.setPositionY(this.getPositionY()+this.veloY);
		if(this.getPositionY()>350){
			this.setPositionY(349);
		}
		else if(this.getPositionY()<150){
			this.setPositionY(151);
		}
},
  flip: function(){
		var audio = cc.audioEngine;
		audio.playEffect('res/sound/jump.mp3');
		this.state *= -1;
		this.move();
},
	move: function(){
		if(this.state<0){
			this.veloY = 20;
		}
		else{
			this.veloY = -20;
		}
	},
	moveRight: function(){
		this.setPositionX(this.getPositionX()+this.veloX);
	},
	moveLeft: function(){
		this.setPositionX(this.getPositionX()-this.veloX);
	},
});
