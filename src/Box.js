var Box = cc.Sprite.extend({
	ctor: function() {
	this._super();
	this.initWithFile( 'res/image/box.png' );
  this.speed = -10;
},
	update: function(dt){
    if(this.getPositionX() <= -100){
      this.random_pos();
    }
    else{
      this.setPositionX(this.getPositionX() + this.speed);
    }
},
  random_pos: function(){
    var pos = Math.round(Math.random()*3);
    this.setPositionX(  1700 );
    if(pos == 1){
      this.setPositionY( 150 );
    }
    else if(pos == 2){
      this.setPositionY( 250 );
    }
    else if(pos == 3){
      this.setPositionY( 350 );
    }
  },
  hitPlayer: function(obj,message){
	if(Math.abs(this.getPositionX()-obj.getPositionX())<=90  &&  Math.abs(this.getPositionY()-obj.getPositionY())<=90){
		this.unscheduleUpdate();
    obj.unscheduleUpdate();
		cc.director.runScene(new LastScene());
    return true;
	}
  else{
    return false;
  }
}
});
